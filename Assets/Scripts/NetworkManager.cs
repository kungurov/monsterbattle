﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Photon;
using UnityEngine;

public class NetworkManager : Photon.MonoBehaviour
{
    static public NetworkManager Instance;
	public const int MaxPlayersInRoom = 2;

    void Start()
    {
        Instance = this;
        InitStartUI();
        InitPhoton();
    }

    public void InitStartUI()
    {
        UIManager.sharedInstance.InitData();
        UIManager.sharedInstance.InitializationRoot(ID_POPUP.MAIN_MENU);
    }

    public void InitPhoton()
    {
        if (PhotonNetwork.connectionStateDetailed == ClientState.PeerCreated)
        { 
            PhotonNetwork.ConnectUsingSettings("1.0.0");
            SetUserName();
            UIManager.sharedInstance.ShowLoadingPanel("Загрузка...");
        }
    }

    public void SetUserName()
    {
        if (String.IsNullOrEmpty(PhotonNetwork.playerName))
        {
            PhotonNetwork.playerName = "Player_" + UnityEngine.Random.Range(1, 10);
        }
    }

    void OnConnectedToMaster()
    {
        UIManager.sharedInstance.HideLoadingPanel();
        PhotonNetwork.JoinLobby();
    }

    void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }

	public void CreateRoom(string name)
	{
		UIManager.sharedInstance.ShowLoadingPanel("Создание комнаты...");
		RoomOptions roomOptions = new RoomOptions() { MaxPlayers = MaxPlayersInRoom };
		PhotonNetwork.CreateRoom(name, roomOptions, null);
	}

    void OnJoinedRoom()
    {
		CheckPlayersInRoom();
		GameMenuViewController gameMenu =  UIManager.PushView(ID_POPUP.GAME_MENU) as GameMenuViewController;
        GameManager.sharedInstance.SetProgressBars(gameMenu);
        GameManager.sharedInstance.CreatePlayer();
    }

	public List<RoomInfo> GetRooms()
	{
		return PhotonNetwork.GetRoomList().ToList();
	}

    public void JoinRoom(string name)
    {
        PhotonNetwork.JoinRoom(name);
    }

    public void CheckPlayersInRoom()
	{
		if (PhotonNetwork.room.PlayerCount == MaxPlayersInRoom) 
		{
			UIManager.sharedInstance.HideLoadingPanel();
		} 
		else 
		{
			UIManager.sharedInstance.ShowLoadingPanel("Ожидание другого игрока...");
		}
	}

	public void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
	{
        CheckPlayersInRoom();
	}

	public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
	{
        GameManager.sharedInstance.UpdatePlayerSession();
        CheckPlayersInRoom();
	}

    public void OnPhotonJoinRoomFailed(object[] cause)
    {
        Debug.Log("Error: Can't join room (full or unknown room name). " + cause[1]);
    }

    public void OnPhotonCreateRoomFailed()
    {
        Debug.Log("Error: Can't create room (room name maybe already used).");
    }

    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("Disconnected from Photon.");
    }

    public void OnFailedToConnectToPhoton(object parameters)
    {
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters + " ServerAddress: " + PhotonNetwork.ServerAddress);
    }		

}
