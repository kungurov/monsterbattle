﻿using UnityEngine;

public class ViewControllerBase : MonoBehaviour
{
    public ID_POPUP ID { get; set; }

    [HideInInspector]
    public UIRoot _uiRoot;

    [HideInInspector]
    public GameObject _viewObject;

    public virtual void onStart()
    {
    }

    public virtual void viewWillAppear()
    {
    }

    public virtual void viewDidAppear()
    {
        _viewObject.SetActive(true);
    }

    public virtual void viewWillDisAppear()
    {
    }

    public virtual void viewDidDisAppear()
    {
        _viewObject.SetActive(false);
    }

    public void unloadView()
    {
        NGUITools.Destroy(_viewObject);
    }
}