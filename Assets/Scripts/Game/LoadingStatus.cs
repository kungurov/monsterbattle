﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingStatus : MonoBehaviour 
{
	public UIPanel panel;
	public UILabel labelStatus;

	public void ShowLoadingPanel(string status)
	{
		panel.gameObject.SetActive(true);
		labelStatus.text = status;
	}

	public void SetStatusLabel(string status)
	{
		labelStatus.text = status;
	}

	public void HideLoadingPanel()
	{
		panel.gameObject.SetActive(false);
	}
}
