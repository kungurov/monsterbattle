﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenuViewController : ViewControllerBase
{
    public UIButton btnCreateRoom;
    public UIButton btnFindRooms;
    public UIInput inputRoomName;

    public override void onStart()
    {
    }

    public override void viewWillAppear()
    {
        base.viewWillAppear();
        btnCreateRoom.isEnabled = true;
        btnFindRooms.isEnabled = true;
        inputRoomName.enabled = true;

        UIEventListener.Get(btnCreateRoom.gameObject).onClick += OnCreateGameClick;
        UIEventListener.Get(btnFindRooms.gameObject).onClick += OnFindRoomsClick;
    }

    public override void viewWillDisAppear()
    {
        base.viewWillDisAppear();
        btnCreateRoom.isEnabled = false;
        btnFindRooms.isEnabled = false;
        inputRoomName.enabled = false;

        UIEventListener.Get(btnCreateRoom.gameObject).onClick -= OnCreateGameClick;
        UIEventListener.Get(btnFindRooms.gameObject).onClick -= OnFindRoomsClick;
    }

    void OnCreateGameClick(GameObject sender)
    {
        string roomName = inputRoomName.value;
        if (!string.IsNullOrEmpty(roomName))
        {
            NetworkManager.Instance.CreateRoom(roomName);
        }
    }

    void OnFindRoomsClick(GameObject sender)
    {
        UIManager.PushView(ID_POPUP.FIND_ROOMS_MENU);
    }
}
