﻿using System.Collections;
using System.Collections.Generic;

public class SpawnPointScript : Photon.PunBehaviour
{
    public UIProgressBar ProgressBar { get; set; }

    public int ID = 1;
    private bool _isFree = true;

    public bool IsFree()
    {
        return _isFree;
    }

    public void SetBusy()
    {
        _isFree = false;
    }

    public void SetFree()
    {
        _isFree = true;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.Serialize(ref _isFree);
        }
        else
        {
            _isFree = (bool)stream.ReceiveNext();
            stream.Serialize(ref _isFree);
        }
    }
}
