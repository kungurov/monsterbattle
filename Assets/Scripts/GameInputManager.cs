﻿using UnityEngine;
using System.Collections;

public class TouchInfo
{
    // точка, относительно которой отсчитывается перемещение
    private Vector2 _center;
    // стартовая точка (в направлении)
    private Vector2 _start;

    // расстояние реакции на свайп
    private float _swipeDelta;
 
    private Touch _touch;

    public TouchInfo(Touch touch, float swipeDelta, float swipeTime)
    {
        _touch = touch;
        _swipeDelta = swipeDelta;
        clear();
    }

    public bool isClick()
    {
        Vector2 deltaPosition = _touch.position - _start;
        if ((deltaPosition.magnitude > _swipeDelta))
        {
            return false;
        }
        return true;
    }

    public void clear()
    {
        _center = _touch.position;
        _start = _center;
    }

    public int FingerId
    {
        get
        {
            return _touch.fingerId;
        }
    }

    public Vector2 Position
    {
        get
        {
            return _touch.position;
        }
    }
}

public class GameInputManager : MonoBehaviour
{
    public Camera gameCamera;

    // учитывать эти данные только при первом свайпе в этом направлении
    private Hashtable _touchesTable;
    private int _firstFingerId;

    public void Start()
    {
        _touchesTable = new Hashtable();
        _firstFingerId = -1;
        _touchesTable.Clear();
    }

    public void Clear()
    {
        _firstFingerId = -1;
        _touchesTable.Clear();
    }

    void checkMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            onSceenTap(Input.mousePosition);
        }
    }

    void onSceenTap(Vector3 position)
    {
        var ray = gameCamera.ScreenPointToRay(position);
        RaycastHit hit;
        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            GamePlayer player = hit.collider.gameObject.GetComponent<GamePlayer>();
            if (player != null)
            {
                player.OnClick();
            }
        }
    }

    void updateKeyboardInput()
    {
        checkMouseClick();
    }

    void updateTouchInput()
    {
        if (_touchesTable != null)
        {
            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        {
                            if (_touchesTable.Count == 0)
                            {
                                _touchesTable[touch.fingerId] = new TouchInfo(touch, 20.0f, 0.1f);
                                _firstFingerId = touch.fingerId;
                            }
                        }
                        break;

                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        {
                            TouchInfo info = (TouchInfo)_touchesTable[touch.fingerId];
                            if (info != null)
                            {
                                if (_touchesTable.Count == 1)
                                {
                                    _touchesTable.Remove(touch.fingerId);
                                    if (info.isClick())
                                    {
                                        onSceenTap(touch.position);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    public void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_WSA
        updateKeyboardInput();
#elif UNITY_IOS || UNITY_ANDROID
        updateTouchInput();
#endif

    }
}
