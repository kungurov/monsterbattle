﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomItemView : MonoBehaviour
{
    public UILabel labelRoomName;
    public UILabel labelUsersNumber;
    public UIButton btnJoin;

    public RoomInfo _room;

    public void SetRoomData(RoomInfo room)
    {
        _room = room;
        if (_room != null)
        {
            labelRoomName.text = _room.Name;
            labelUsersNumber.text = _room.PlayerCount.ToString() + "/" + _room.MaxPlayers.ToString();
            SetJoinButton();
        }
    }

    private void SetJoinButton()
    {
        if (_room.PlayerCount < _room.MaxPlayers)
        {
            UIEventListener.Get(btnJoin.gameObject).onClick = OnJoinClick;
        }
        else
        {
            btnJoin.enabled = false;
        }
    }

    void OnJoinClick(GameObject sender)
    {
        NetworkManager.Instance.JoinRoom(_room.Name);
    }
}
