﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : Singleton<GameManager>
{
    private List<string> _availableCharacters;

    private GamePlayer _gamePlayer;
    private List<GamePlayer> _otherPlayers;

    public SpawnPointScript[] spawnPoints;

    void Start()
    {
        _otherPlayers = new List<GamePlayer>();
        SetAvailableCharacters();
    }

    public void SetAvailableCharacters()
    {
        _availableCharacters = new List<string>();
        _availableCharacters.Add("Prefabs/Game/GamePlayerDron");
        _availableCharacters.Add("Prefabs/Game/GamePlayerWarrior");
    }

    public void CreatePlayer()
    {
        StartCoroutine(CreatePlayerWithDelay(1.0f));
    }

    private IEnumerator CreatePlayerWithDelay(float delay)
	{
        yield return new WaitForSeconds(delay);
        SpawnPointScript spawnPoint = GetFreeSpawnPoint();
        if (spawnPoint != null)
        {
            Debug.Log(spawnPoint.gameObject.name.ToString() + " " + spawnPoint.ID.ToString());
            _gamePlayer = PhotonNetwork.Instantiate(_availableCharacters[UnityEngine.Random.Range(0, _availableCharacters.Count)], 
                spawnPoint.transform.position,
                 spawnPoint.transform.rotation, 
                0).GetComponent<GamePlayer>();
            _gamePlayer.SetSpawnPointID(spawnPoint.ID);
        }
    }

    public void AddOtherPlayer(GamePlayer player)
    {
        if (!player.photonView.isMine)
        {
            _otherPlayers.Add(player);
        }
    }

    public void RemoveOtherPlayer(GamePlayer player)
    {
        _otherPlayers.Remove(player);
    }

    public void HitOtherPlayers()
    {
        foreach (GamePlayer player in _otherPlayers)
        {
            player.ReduceRpc();
        }
    }

    public void FreeSpawnPoint()
    {
        if (_gamePlayer != null)
        {
            _gamePlayer.FreeSpawnPoint();
        }
    }

    public SpawnPointScript GetSpawnPointByID(int ID)
    {
        return spawnPoints.ToList().FindAll(point => point.ID == ID).First();
    }

    public SpawnPointScript GetFreeSpawnPoint()
    {
        return spawnPoints.ToList().FindAll(point => point.IsFree()).First();
    }

    public void UpdatePlayerSession()
    {
        foreach (SpawnPointScript points in spawnPoints)
        {
            points.SetFree();
        }
        _gamePlayer.SetSpawnPointID(GetFreeSpawnPoint().ID);
    }

    public void SetProgressBars(GameMenuViewController gameMenu)
    {
        if (gameMenu != null)
        {
            for (int i = 0; i < spawnPoints.Length; i++)
            {
                if (i < gameMenu.progressBars.Length)
                {
                    spawnPoints[i].ProgressBar = gameMenu.progressBars[i];
                }
            }
        }
    }
}
