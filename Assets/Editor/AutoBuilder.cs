
using System;
using UnityEngine;
using UnityEditor;
using System.IO;

public static class AutoBuilder
{
    static string projectName = "Monster Battle";
    public static string bundleVersion = "1.0.0";
    static int bundleVersionCode = 1;
    static string companyName = "Artem Kungurov";	
	static string bundleIdentifier = "com.curvedfinger.dinogo";
	static string keyPass = "xn9fcw";

    // Android
    static string companyNameAndroid = "Artem Kungurov";
    static string bundleIdentifierAndroid = "com.curvedfinger.dinogo";
    static string keyStoreNameAndroid = "android/release_key.keystore";
    static string keyaliasNameAndroid = "dinogo";

    static bool isDebug = false;

	static string GetNameBuildAndroid()
	{
		string result =	projectName.Replace(" ", "_");
		result += "_android_" + bundleVersion.ToString() + "_(" + bundleVersionCode.ToString() + ")";
		return result;
	}

    private static string GetOutputPath(string defaultPath)
    {
        const string outputPathCommandArg = "-outputPath";
        String[] commandLineArgs = System.Environment.GetCommandLineArgs();
        int outputPathIndex = -1;
        for (int i = 0; i < commandLineArgs.Length; i++)
        {
            if (commandLineArgs[i].Equals(outputPathCommandArg))
            {
                outputPathIndex = i;
            }
        }
        string outputPath = defaultPath;
        if (outputPathIndex >= 0 && outputPathIndex < commandLineArgs.Length - 1)
        {
            outputPath = commandLineArgs[outputPathIndex + 1];
        }
        CheckFilePathForWriting(outputPath);
        return outputPath;
    }

    private static void CheckFilePathForWriting(string filePath)
    {
        if (!filePath.Contains("/"))
        {
            return;
        }
        filePath = filePath.Substring(0, filePath.LastIndexOf('/'));
        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }
    }

    static string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        return s[s.Length - 2];
    }

    static string[] GetScenePaths()
    {
        string[] scenes = new string[EditorBuildSettings.scenes.Length];

        for (int i = 0; i < scenes.Length; i++)
        {
            scenes[i] = EditorBuildSettings.scenes[i].path;
        }

        return scenes;
    }

	static void SetIcons(BuildTargetGroup platformGroup, string platformName)
    {
        int[] iconsSize = PlayerSettings.GetIconSizesForTargetGroup(platformGroup);
        Texture2D[] icons = new Texture2D[iconsSize.Length];
        string iconPath = "";
        for (int i = 0; i < iconsSize.Length; i++)
        {
            iconPath = "Assets/Icons/" + platformName + "/icon-" + iconsSize[i].ToString() + ".png";
            icons[i] = AssetDatabase.LoadMainAssetAtPath(iconPath) as Texture2D;
        }
        PlayerSettings.SetIconsForTargetGroup(platformGroup, icons);
    }

    static void SetAndroidSettings(int mode = 0)
    {
        PlayerSettings.productName = projectName;
        PlayerSettings.bundleVersion = bundleVersion;
        PlayerSettings.Android.bundleVersionCode = bundleVersionCode;
        PlayerSettings.keyaliasPass = keyPass;
        PlayerSettings.keystorePass = keyPass;
        switch (mode)
        {
            case 0:
                {
                    PlayerSettings.Android.targetDevice = AndroidTargetDevice.FAT;
                    break;
                }
            case 1:
                {
                    PlayerSettings.Android.targetDevice = AndroidTargetDevice.ARMv7;
                    break;
                }
            case 2:
                {
                    PlayerSettings.Android.targetDevice = AndroidTargetDevice.x86;
                    break;
                }
        }

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
    }

    [MenuItem("AutoBuilder/Debug/Android")]
    static void PerformAndroidBuildDebug()
    {
        isDebug = true;
        PerformAndroidBuildFat();
        isDebug = false;
    }

    [MenuItem("AutoBuilder/Release/Android_ARM")]
    static void PerformAndroidBuildARM()
    {
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "PLATFORM_GOOGLEPLAY" + (isDebug ? ",DEBUG" : ""));
        PlayerSettings.companyName = companyNameAndroid;        
        PlayerSettings.applicationIdentifier = bundleIdentifierAndroid;     
        PlayerSettings.Android.keystoreName = keyStoreNameAndroid;       
        PlayerSettings.Android.keyaliasName = keyaliasNameAndroid;
        SetAndroidSettings(1);
		BuildPipeline.BuildPlayer(GetScenePaths(), GetOutputPath("Builds/android/" + GetNameBuildAndroid() + ".apk"), BuildTarget.Android, BuildOptions.None);
    }

    [MenuItem("AutoBuilder/Release/Android_X86")]
    static void PerformAndroidBuildIntel()
    {
        SetIcons(BuildTargetGroup.Android, "android");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "PLATFORM_GOOGLEPLAY" + (isDebug ? ",DEBUG" : ""));
        PlayerSettings.companyName = companyNameAndroid;
        PlayerSettings.applicationIdentifier = bundleIdentifierAndroid;
        PlayerSettings.Android.keystoreName = keyStoreNameAndroid;
        PlayerSettings.Android.keyaliasName = keyaliasNameAndroid;
		PlayerSettings.Android.useAPKExpansionFiles = false;
        SetAndroidSettings(2);
		BuildPipeline.BuildPlayer(GetScenePaths(), GetOutputPath("Builds/android/"+ GetNameBuildAndroid() + ".apk"), BuildTarget.Android, BuildOptions.None);
    }

    [MenuItem("AutoBuilder/Release/Android_FAT")]
    static void PerformAndroidBuildFat()
    {
        SetIcons(BuildTargetGroup.Android, "android");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "PLATFORM_GOOGLEPLAY" + (isDebug ? ",DEBUG" : ""));
        PlayerSettings.companyName = companyNameAndroid;
        PlayerSettings.applicationIdentifier = bundleIdentifierAndroid;
        PlayerSettings.Android.keystoreName = keyStoreNameAndroid;
        PlayerSettings.Android.keyaliasName = keyaliasNameAndroid;
        SetAndroidSettings();
		BuildPipeline.BuildPlayer(GetScenePaths(), GetOutputPath("Builds/android/" + GetNameBuildAndroid() + ".apk"), BuildTarget.Android, BuildOptions.None);
    }
}