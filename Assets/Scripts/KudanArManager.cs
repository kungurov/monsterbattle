﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudan.AR;

public class KudanArManager : Singleton<KudanArManager>
{
    public KudanTracker _kudanTracker;
    public TrackingMethodMarker _markerTracking;
    public TrackingMethodMarkerless _markerlessTracking;

    public void SetMarker()
    {
        _kudanTracker.ChangeTrackingMethod(_markerTracking);
    }

    public void SetMarkerless()
    {
        _kudanTracker.ChangeTrackingMethod(_markerlessTracking);
    }

    public void StartClicked()
    {
        if (!_kudanTracker.ArbiTrackIsTracking())
        {
            Vector3 floorPosition;
            Quaternion floorOrientation;

            _kudanTracker.FloorPlaceGetPose(out floorPosition, out floorOrientation);
            _kudanTracker.ArbiTrackStart(floorPosition, floorOrientation);
        }
        else
        {
            _kudanTracker.ArbiTrackStop();
        }
    }
}
