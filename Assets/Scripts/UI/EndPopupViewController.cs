﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPopupViewController : ViewControllerBase 
{
	[SerializeField]
	private UILabel labelStatus;

	public void ShowEndGameStatus(string status)
	{
		labelStatus.text = status;
	}
}
