﻿using System.Collections;
using System.Collections.Generic;
using Photon;
using UnityEngine;

public class GamePlayer : Photon.MonoBehaviour
{
    public GameObject minePointer;

    private int SpawnPointID;
    private SpawnPointScript _spawnPoint;

    public const float MaxHealth = 100;
    public const float DamageValue = 10;

    public float _currentHealth = 100;

    public void Awake()
    {
        GameManager.sharedInstance.AddOtherPlayer(this);
        InitMine();
    }

    public void SetSpawnPointID(int ID)
    {
        SpawnPointID = ID;
        _spawnPoint = GameManager.sharedInstance.GetSpawnPointByID(SpawnPointID);
        SetTransformFromSpawn();
    }

    public void InitMine()
    {
        minePointer.SetActive(photonView.isMine);
        if (!photonView.isMine)
        {
            _spawnPoint = GameManager.sharedInstance.GetFreeSpawnPoint();
            SetTransformFromSpawn();
        }
    }

    public void SetTransformFromSpawn()
    {
        transform.parent = _spawnPoint.transform.parent.transform;
        transform.localPosition = _spawnPoint.transform.localPosition;
        transform.rotation = _spawnPoint.transform.rotation;
        if (_spawnPoint != null)
        {
            _spawnPoint.SetBusy();
        }
    }

    public void FreeSpawnPoint()
    {
        if (_spawnPoint != null)
        {
            _spawnPoint.SetFree();
        }
    }

    public void OnClick()
    {
        Debug.Log("Click");
        if (photonView.isMine)
        {
            GameManager.sharedInstance.HitOtherPlayers();
        }
    }

    public void ReduceRpc()
    {
		photonView.RPC("ReduceHealth", PhotonTargets.All);
    }

    public void Reduce(float value)
    {
        _currentHealth -= value;
        UpdateProgressBar();
		if (_currentHealth <= 0) 
		{
			EndGame(photonView.isMine ? "Вы проиграли!" : "Вы победили!");
		}
    }

    public void UpdateProgressBar()
    {
        if (_spawnPoint != null)
        {
            if (_spawnPoint.ProgressBar != null)
            {
                _spawnPoint.ProgressBar.value = _currentHealth / MaxHealth;
            }
        }
    }

    [PunRPC]
    public void ReduceHealth()
    {
        Reduce(DamageValue);
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
    }

    void OnDestroy()
    {
        if (GameManager.sharedInstance != null)
        {
            GameManager.sharedInstance.RemoveOtherPlayer(this);
        }
    }

	public void EndGame(string status)
	{
		EndPopupViewController endMenu = UIManager.PushView(ID_POPUP.END_MENU) as EndPopupViewController;
		if (endMenu != null) 
		{
			endMenu.ShowEndGameStatus(status);
		}
	}
}
