﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindRoomsViewController : ViewControllerBase
{
    public GameObject roomItem;
    public UIGrid grid;
    public UIScrollView scrollView;
    public UILabel labelStatus;

    public UIButton btnBack;
    public UIButton btnRefresh;

    public override void onStart()
    {
        RequestPage();
    }

    public override void viewWillAppear()
    {
        base.viewWillAppear();
        btnBack.isEnabled = true;
        btnRefresh.isEnabled = true;

        UIEventListener.Get(btnBack.gameObject).onClick += OnBackClick;
        UIEventListener.Get(btnRefresh.gameObject).onClick += OnRefreshClick;
    }

    public override void viewWillDisAppear()
    {
        base.viewWillDisAppear();
        btnBack.isEnabled = false;
        btnRefresh.isEnabled = false;

        UIEventListener.Get(btnBack.gameObject).onClick -= OnBackClick;
        UIEventListener.Get(btnRefresh.gameObject).onClick -= OnRefreshClick;
    }

    void OnBackClick(GameObject sender)
    {
        UIManager.PopView();
    }

    void OnRefreshClick(GameObject sender)
    {
        HideStatusLabel();
        ClearServerItems();
        RequestPage();
    }

    public void RequestPage()
    {
        List<RoomInfo> rooms = NetworkManager.Instance.GetRooms();
        if (rooms.Count == 0)
        {
            ShowStatusLabel("Нет доступных комнат");
        }
        else
        {
            ShowRoomItems(rooms);
        }
    }

    private void ShowStatusLabel(string text)
    {
        labelStatus.gameObject.SetActive(true);
        labelStatus.text = text;
    }

    private void HideStatusLabel()
    {
        labelStatus.gameObject.SetActive(false);
    }

    private void ShowRoomItems(List<RoomInfo> rooms)
    {
        if (rooms != null)
        {
            for (int i = 0; i < rooms.Count; i++)
            {
                RoomItemView item = NGUITools.AddChild(grid.gameObject, roomItem).GetComponent<RoomItemView>();
                item.SetRoomData(rooms[i]);
            }
            grid.Reposition();
            scrollView.ResetPosition();
        }
    }

    private void ClearServerItems()
    {
        foreach (Transform child in grid.GetChildList())
        {
            NGUITools.Destroy(child.gameObject);
        }
    }
}
